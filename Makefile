# WARNING: `make` *requires* the use of tabs, not spaces, at the start of each command

PYTHON_INTERPRETER = env/bin/python
PIP = env/bin/pip

default: help

.PHONY: help
help: # Help command
	@sed -nE 's/^([a-zA-Z0-9_-]+):.*#'  Makefile | sort | while read -r l; do printf "\033[1;32m$$(echo $$l | cut -f 1 -d':')\033[00m:$$(echo $$l | cut -f 2- -d'#')\n"; done

.PHONY: format
format: # Formats
	yapf --recursive --in-place --parallel --verbose app

.PHONY: lint
lint: # Run linter, then exit with an error code if needed
	flake8 app/
	yapf --recursive --diff --parallel app

.PHONY: test
test: # Run pytest
	pytest tests/test_main.py

.PHONY: start
start: # Start application
	uvicorn app.main:app --reload

.PHONY: install
install: # Install the project dependencies defined in the requirements.txt file.
	$(PIP) install -r requirements.txt

.PHONY: clean
clean: # remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

flag: # print the flag
	echo "bmljZSB0cnkhIFRoZSBmbGFnIGlzIG5vdCBoZXJl" | base64 --decode
